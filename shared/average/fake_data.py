import requests
from faker import Faker
import random

fake = Faker()

# create courses
courses = [
    {"term": 1169, "subject":"CS", "course":"483", "section":"1"},
    {"term": 1169, "subject":"ECE", "course":"482", "section":"2"},
    {"term": 1169, "subject":"ECE", "course":"453", "section":"3"},
    {"term": 1169, "subject":"CS", "course":"169", "section":"4"},
    {"term": 1169, "subject":"MATH", "course":"122", "section":"5"},
    {"term": 1169, "subject":"ECON", "course":"484", "section":"6"},
    {"term": 1169, "subject":"MATH", "course":"316", "section":"7"},
    {"term": 1169, "subject":"ECE", "course":"308", "section":"8"},
    {"term": 1169, "subject":"ECE", "course":"459", "section":"9"},
    {"term": 1169, "subject":"ECE", "course":"406", "section":"10"}
]

for c in courses:
    payload = c
    r = requests.post("http://localhost:8088/studentsApi/v1/course", json=payload)

created_ids = {}
unique_names = {}
programs = ["CE", "SE", "CS", "MATH", "EE"]
level = ["1A","1B","2A", "2B","3A", "3B","4A", "4B"]
amount = range(1,10000)

for i in amount:
    sid = i
    # generate unique id
    uw_id = random.randint(20000000, 30000000)
    while uw_id in created_ids:
        uw_id = random.randint(20000000, 30000000)
    created_ids[uw_id] = 1

    # generate unique name
    name = fake.name().split(' ')
    fname = str(name[0])
    lname = str(name[1])
    n = fname + lname
    while n in unique_names:
        name = fake.name().split(' ')
        fname = str(name[0])
        lname = str(name[1])
        n = fname + lname
    unique_names[n] = 1

    p = random.choice(programs)
    lvl = random.choice(level)

    payload = {
        "internal_id":-1,
        "uwid": uw_id,
        "firstname": fname,
        "lastname":lname
    }
    r = requests.post("http://localhost:8088/studentsApi/v1/student", json=payload)

    payload = {
        "student_internal_id": sid,
        "term":"1169",
        "program":p,
        "level":lvl
    }
    r = requests.post("http://localhost:8088/studentsApi/v1/student_enrolment", json=payload)

    for j in range(3):
        payload = {
            "student_internal_id": sid,
            "course_internal_id": int(random.choice(range(1,11))),
            "mark": str(int(random.gauss(70, 20)))
        }
        
    r = requests.post('http://localhost:8088/studentsApi/v1/course_mark', json=payload)

print "Done"
