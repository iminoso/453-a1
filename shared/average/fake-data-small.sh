curl -H "Content-Type: application/json" -d '{"internal_id":-1, "uwid":"20292648", "firstname":"Lisa", "lastname":"Smith"}' http://localhost:8088/studentsApi/v1/student
curl -H "Content-Type: application/json" -d '{"student_internal_id":1, "term":"1169", "program":"SE", "level":"1A"}' http://localhost:8088/studentsApi/v1/student_enrolment
curl -H "Content-Type: application/json" -d '{"internal_id":-1, "uwid":"20701819", "firstname":"Shelby", "lastname":"Taylor"}' http://localhost:8088/studentsApi/v1/student
curl -H "Content-Type: application/json" -d '{"student_internal_id":2, "term":"1169", "program":"ELE", "level":"1A"}' http://localhost:8088/studentsApi/v1/student_enrolment
curl -H "Content-Type: application/json" -d '{"internal_id":-1, "uwid":"20234201", "firstname":"Mary", "lastname":"Williams"}' http://localhost:8088/studentsApi/v1/student
curl -H "Content-Type: application/json" -d '{"student_internal_id":3, "term":"1169", "program":"COMPE", "level":"1A"}' http://localhost:8088/studentsApi/v1/student_enrolment
curl -H "Content-Type: application/json" -d '{"internal_id":-1, "uwid":"20901499", "firstname":"Victoria", "lastname":"Gardner"}' http://localhost:8088/studentsApi/v1/student
curl -H "Content-Type: application/json" -d '{"student_internal_id":4, "term":"1169", "program":"CS", "level":"1A"}' http://localhost:8088/studentsApi/v1/student_enrolment
curl -H "Content-Type: application/json" -d '{"internal_id":-1, "uwid":"20825454", "firstname":"Wesley", "lastname":"Garcia"}' http://localhost:8088/studentsApi/v1/student
curl -H "Content-Type: application/json" -d '{"student_internal_id":5, "term":"1169", "program":"COMPE", "level":"1A"}' http://localhost:8088/studentsApi/v1/student_enrolment

curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"MATH", "course":"122", "section":"5"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"CS", "course":"483", "section":"1"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"ECE", "course":"482", "section":"3"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"CS", "course":"169", "section":"6"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"ECON", "course":"484", "section":"6"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"ECE", "course":"308", "section":"8"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"MATH", "course":"316", "section":"7"}' http://localhost:8088/studentsApi/v1/course
curl -H "Content-Type: application/json" -d '{"term": 1169, "subject":"MATH", "course":"257", "section":"3"}' http://localhost:8088/studentsApi/v1/course

curl -H "Content-Type: application/json" -d '{"student_internal_id": 1, "course_internal_id": 8, "mark": "76"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 1, "course_internal_id": 1, "mark": "73"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 1, "course_internal_id": 5, "mark": "63"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 1, "course_internal_id": 2, "mark": "86"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 1, "course_internal_id": 4, "mark": "74"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 2, "course_internal_id": 3, "mark": "83"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 2, "course_internal_id": 1, "mark": "66"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 2, "course_internal_id": 6, "mark": "84"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 2, "course_internal_id": 2, "mark": "67"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 2, "course_internal_id": 2, "mark": "66"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 3, "course_internal_id": 7, "mark": "67"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 3, "course_internal_id": 3, "mark": "94"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 3, "course_internal_id": 8, "mark": "70"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 3, "course_internal_id": 2, "mark": "93"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 3, "course_internal_id": 8, "mark": "71"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 4, "course_internal_id": 5, "mark": "54"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 4, "course_internal_id": 2, "mark": "58"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 4, "course_internal_id": 6, "mark": "71"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 4, "course_internal_id": 5, "mark": "65"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 4, "course_internal_id": 2, "mark": "73"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 5, "course_internal_id": 4, "mark": "74"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 5, "course_internal_id": 4, "mark": "92"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 5, "course_internal_id": 2, "mark": "77"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 5, "course_internal_id": 4, "mark": "90"}' http://localhost:8088/studentsApi/v1/course_mark
curl -H "Content-Type: application/json" -d '{"student_internal_id": 5, "course_internal_id": 7, "mark": "98"}' http://localhost:8088/studentsApi/v1/course_mark
