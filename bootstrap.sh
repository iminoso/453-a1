#!/usr/bin/env bash
case $(id -u) in
    0)
        apt-get update
        apt-get install -y default-jdk maven git

        # from the Dart install instructions: installing Dart in 3 Steps
        sh -c 'curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -'
        sh -c 'curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'
        apt-get update
        apt-get install dart

        sudo -u vagrant -i $0
        ;;
    *)
	echo "export PATH=/usr/lib/dart/bin:$PATH" > ~/.bash_profile

        ;;
esac
